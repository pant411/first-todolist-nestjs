import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './DTO/tasks.dto';
import { Task } from 'src/entities/task.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class TasksService {
    private tasks: Task[] = [];
    private autoNumber = 1;

    constructor(
        @InjectRepository(Task)
        private taskRepository: Repository<Task>,
    ) { }

    async create(data: CreateTaskDto) {
        const newItem = this.taskRepository.create(data);
        return this.taskRepository.save(newItem);
    }

    async find() {
        const [data, count] = await this.taskRepository.findAndCount({
            order: { createdAt: 'ASC' },
        });
        return { data, count };
    }

    async findById(taskId: string) {
        return this.taskRepository.findOneOrFail(taskId).catch(() => {
            throw new NotFoundException('Task is not found');
        });
    }

    async update(taskId: string, data: CreateTaskDto) {
        const item = await this.taskRepository.findOneOrFail(taskId).catch(() => {
            throw new NotFoundException('Task is not found');
        });
        return this.taskRepository.save({ ...item, ...data });
    }

}
