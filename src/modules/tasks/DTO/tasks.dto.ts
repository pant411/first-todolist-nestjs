import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsOptional, IsString, IsBoolean } from "class-validator";

export class CreateTaskDto {
    @ApiPropertyOptional()
    @IsString()
    @IsOptional()
    id?: string;

    @ApiProperty()
    @IsString()
    title: string;

    @ApiProperty()
    @IsString()
    content: string;

    @ApiProperty()
    @IsBoolean()
    isDone: boolean;

}