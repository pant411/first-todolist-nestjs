import { Body, Controller, Get, Param, ParseIntPipe, Post, Patch, Delete } from '@nestjs/common';
import { ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { CreateTaskDto } from './DTO/tasks.dto';
import { TasksService } from './tasks.service';

@ApiTags('tasks')
@Controller('tasks')
export class TasksController {

    constructor(private tasksService: TasksService) { }

    @Post()
    @ApiCreatedResponse({ type: CreateTaskDto })
    create(@Body() data: CreateTaskDto) {
        return this.tasksService.create(data);
    }

    @Get()
    find() {
        return this.tasksService.find();
    }

    @Get(':id')
    findById(@Param('id') taskId: string) {
        return this.tasksService.findById(taskId);
    }

    @Patch(':id')
    async update(@Param('id') taskId: string, @Body() data: CreateTaskDto) {
        return this.tasksService.update(taskId, data);
    }

}
